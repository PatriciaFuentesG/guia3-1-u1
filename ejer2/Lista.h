#ifndef LISTA_H
#define LISTA_H

#include <iostream>
#include<cstring>
using namespace std;



/* define la estructura del nodo. */
typedef struct _Nodo {

    string palabra;
    struct _Nodo *sig;
} Nodo;

class Lista {
    private:
        Nodo *principio = NULL;
        Nodo *ultimo = NULL;
        Nodo *antes = NULL;
        Nodo *actual = NULL;
        Nodo *despues = NULL;
      

    public:
        /* constructor*/
        Lista();
           
   
        void imprimir ();
        void lista_orden(string palabra);
        

      
};
#endif
