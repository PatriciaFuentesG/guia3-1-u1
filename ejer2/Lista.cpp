#include <iostream>
using namespace std;

#include "Lista.h"
#include<cstring>

Lista::Lista() {}


//Esta funcion sirve para saber si el nuevo elemento es mayor a los ya existentes


void Lista::imprimir () {
  
    Nodo *tmp = this->principio;
    cout << endl;

    while (tmp != NULL) {
        cout << "Palabra:"<< tmp->palabra << endl;
        tmp = tmp->sig; 
    }
    cout << endl;
}
//Esta funcion se encarga aparte de agregar elementos, es agregarlos en orden , buscando su ubicacion dentro de los nodos ya existentes

void Lista::lista_orden(string palabra){

    cout << endl;
    Nodo *tmp;
    tmp = new Nodo;
    tmp->sig = NULL;
    tmp->palabra = palabra;
    this->actual = tmp;
    bool mayor = false;
   
   

    if (this->principio == NULL) { 
            
        this->principio = tmp;
        this->ultimo = this->principio;
    }else{
              
        if(strcmp(this->actual->palabra.c_str(),this->ultimo->palabra.c_str())>0){//Se uso la funcion strncmp para saber el orden de las letras

             this->ultimo->sig = tmp;
            this->ultimo = tmp;
        }else{
            tmp = this->principio;
            while(tmp != NULL && mayor == false){

                if(strcmp(this->actual->palabra.c_str(),tmp->palabra.c_str())<0){
                    mayor = true;
                    this->despues=tmp;
                    break;
                }else{
                    this->antes = tmp;
                    tmp = tmp->sig;
                }

            }
            if(strcmp(tmp->palabra.c_str(), this->principio->palabra.c_str())== 0){
                this->principio = this->actual;
                this->actual->sig=tmp;
                this->principio->sig =tmp;
            }else{
                this->antes->sig = this->actual;
                this->actual->sig = tmp;
            }

        }
    }

   


}
