#include <iostream>
using namespace std;

#include "Lista.h"
#include <time.h>

Lista::Lista() {}

//Esta funcion se encarga aparte de agregar elementos, es agregarlos en orden , buscando su ubicacion dentro de los nodos ya existentes
void Lista::crear_lista_ordenada (int numero) {
    Nodo *tmp;
    tmp = new Nodo;
    tmp->sig = NULL;
    tmp->numero = numero;
    bool mayor = false;
    this->actual = tmp;
    
           
    if (this->principio == NULL) { 
            
        this->principio = tmp;
        this->ultimo = this->principio;
  
    }else{  
        
             if(orden(numero) == false){
                
                 this->ultimo->sig = tmp;
                 this->ultimo = tmp;
             }else{
             
             tmp = this->principio;
             while(tmp != NULL && mayor == false) {
    
                 
                 if(numero < tmp->numero ){

                     mayor = true;
                     this->despues = tmp;
                     break;
            
                }else{
                  
                      this->antes = tmp;  
                      tmp = tmp->sig;
                                           
                    }
    
             } 
             if(tmp == this->principio){
                 this->principio = this->actual;
                 this->actual->sig = tmp;
                 this->principio->sig = tmp;
             }else{
                
                 this->antes->sig = this->actual;
                 this->actual->sig = tmp;
              
             }     
            
         }       
   
  }
}


void Lista::agregar(int numero){
    Nodo *tmp;
    tmp = new Nodo;
    tmp->sig = NULL;
    tmp->numero = numero;

    if (this->principio == NULL) { 
            
        this->principio = tmp;
        this->ultimo = this->principio;
    }else{
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
  

}

//Esta funcion sirve para saber si el nuevo elemento es mayor a los ya existentes
bool Lista::orden(int numero){
    Nodo *tmp = this->ultimo;
    bool posicion = false;
    if(numero < tmp->numero ){

         posicion = true;
           
    }
    
    return posicion;


}

void Lista::imprimir () {
  
    Nodo *tmp = this->principio;
    cout << endl;

    while (tmp != NULL) {
        cout << "numero:"<< tmp->numero << endl;
        tmp = tmp->sig; 
    }
    cout << endl;
}
//Separa los numeros positivos y negativos en dos listas
void Lista::separar(){

    Lista *positivos = new Lista();
    Lista *negativos = new Lista();

    Nodo *tmp = this->principio;
    cout << endl;

    while (tmp != NULL) {
        if(tmp->numero >= 0){//al 0 sera considerado dentro de los positivos
            positivos->crear_lista_ordenada(tmp->numero);

        }else{
            negativos->crear_lista_ordenada(tmp->numero);
        }
        tmp = tmp->sig; 
    }
   

    cout << "Numeros positivos" << endl;

    positivos->imprimir();

    cout << "Numeros negativos"<< endl;
    negativos->imprimir();

    delete positivos;
    delete negativos;
    

}
