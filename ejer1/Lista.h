#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using namespace std;



/* define la estructura del nodo. */
typedef struct _Nodo {
    
    int numero = 0;
    struct _Nodo *sig;
} Nodo;

class Lista {
    private:
        Nodo *principio = NULL;
        Nodo *ultimo = NULL;
        Nodo *antes = NULL;
        Nodo *actual = NULL;
        Nodo *despues = NULL;
      

    public:
        /* constructor*/
        Lista();
        

        void crear_lista_ordenada (int numero);

        void imprimir ();

        bool orden(int numero);

        void agregar(int numero);

        void separar();

     
};
#endif
